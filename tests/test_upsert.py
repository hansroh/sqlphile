from sqlphile import Q, D, F, V
import pytest

def test_upsert (sqlmaps):
    sql = (sqlmaps.insert ("tbl1 a")
        .data (a = 1, b = 1)
        .upflict ('a', b = F ('b + 1')))

    assert str (sql) == (
        "INSERT INTO tbl1 a (a, b) VALUES (1, 1)\n"
        "ON CONFLICT (a) DO UPDATE SET b = b + 1"
    )

    sql = (sqlmaps.insert ("tbl1 a")
        .data (a = 1, b = 1)
        .upflict ('a')
        .returning ('id'))

    assert str (sql) == (
        "INSERT INTO tbl1 a (a, b) VALUES (1, 1)\n"
        "ON CONFLICT (a) DO NOTHING\n"
        'RETURNING id'
    )

    with pytest.raises (AssertionError):
        sql = (sqlmaps.select ("tbl1 a")
            .upflict ('a', b = F ('b + 1')))

