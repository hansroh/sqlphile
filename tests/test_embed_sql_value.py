from sqlphile import Q, D, F, V

def test_embed_sql (sqlmaps):
    embed = sqlmaps.select ('kk').filter (id=1)
    sql = (
        sqlmaps.select ("temp").filter (id = embed)
    )
    assert str (sql) == (
        "SELECT * FROM temp\n"
        "WHERE id = (SELECT * FROM kk\n"
        "WHERE id = 1)"
    )


