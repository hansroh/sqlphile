from sqlphile import Q, D, F, V

def test_insert_select (sqlmaps):
    sql = (sqlmaps.select ("tbl1 a")
        .join ("tbl2 b", "a.id = b.id")
        .filter (a__id = 4)
        .get ("a.id, b.name")
        .into ("tbl3 (id, name)"))

    assert str (sql) == (
        "INSERT INTO tbl3 (id, name)\n"
        "SELECT a.id, b.name FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )
    