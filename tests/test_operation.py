from sqlphile import Q, D, F, V
import datetime

def test_operation (sqlmaps):
	sql = sqlmaps.insert ("rc_file")
	sql.data (_id = 1, score = 1.3242, name = "file-A", moddate = datetime.date.today ())
	assert sql.query.find ("INSERT INTO rc_file") !=- 1
	assert sql.query.find (") VALUES (") !=- 1

	sql = sqlmaps.update ("rc_file")
	sql.data (score = 1.3242, name = "file-A", moddate = datetime.date.today ())
	sql.filter (email = None, _id = 1, name__contains = "Hans Roh", moddate = datetime.date.today ()).order_by ("name", "-type").group_by ("name")[10:20].having ("count(*) > 10")

	assert sql.query.find ("moddate = TIMESTAMP '") != 1
	assert sql.query.find ("UPDATE rc_file SET") !=- 1
	assert sql.query.find ("score = 1.3242") !=- 1
	assert sql.query.find ("WHERE") !=- 1
	assert sql.query.find ("GROUP BY name") !=- 1
	assert sql.query.find ("LIMIT 10") !=- 1
	assert sql.query.find ("OFFSET 10") !=- 1
	assert sql.query.find ("HAVING count(*) > 10") !=- 1

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql.filter (email = None, _id = 1, name__contains = "Hans Roh").order_by ("name", "-type").group_by ("name")[10:20].having ("count(*) > 10")
	assert sql.query.find ("SELECT score, count(*) cnt FROM rc_file") !=- 1
	assert sql.query.find ("WHERE") !=- 1
	assert sql.query.find ("GROUP BY name") !=- 1
	assert sql.query.find ("LIMIT 10") !=- 1
	assert sql.query.find ("OFFSET 10") !=- 1
	assert sql.query.find ("HAVING count(*) > 10") !=- 1

	sql = sqlmaps.delete ("rc_file")
	sql.filter (email = None, _id = 1, name__contains = "Hans Roh").order_by ("name", "-type").group_by ("name")[10:20].having ("count(*) > 10")
	assert sql.query.find ("DELETE FROM rc_file") !=- 1
	assert sql.query.find ("WHERE") !=- 1
	assert sql.query.find ("GROUP BY name") !=- 1
	assert sql.query.find ("LIMIT 10") !=- 1
	assert sql.query.find ("OFFSET 10") !=- 1
	assert sql.query.find ("HAVING count(*) > 10") !=- 1

	sql = sqlmaps.select ("rc_file", "t1").join ("names", "t2", t1__name = F ("t2.name"))
	sql.get ("score", "t2.name")
	sql.filter (email = None, _id = 1).order_by ("t2.name", "-type").group_by ("t2.name")[10:20].having ("count(*) > 10")
	assert sql.query.find ("SELECT score, t2.name FROM rc_file AS t1") !=- 1
	assert sql.query.find ("INNER JOIN names AS t2 ON t1.name = t2.name") !=- 1
	assert sql.query.find ("WHERE") !=- 1
	assert sql.query.find ("GROUP BY t2.name") !=- 1
	assert sql.query.find ("LIMIT 10") !=- 1
	assert sql.query.find ("OFFSET 10") !=- 1
	assert sql.query.find ("HAVING count(*) > 10") !=- 1

	sql = sqlmaps.select ("rc_file", "t1").join (sql, "t2", t1__name = F ("t2.name"))
	sql.get ("score", "t2.name")
	sql.filter (email = None, _id = 1).order_by ("t2.name", "-type").group_by ("t2.name")[10:20].having ("count(*) > 10")
	assert sql.query.find ("INNER JOIN (SELECT") !=- 1
	assert sql.query.find (") AS t2 ON t1.name = t2.name") !=- 1

	subq = sqlmaps.select ("rc_project")
	assert subq.query == "SELECT * FROM rc_project"

	subq = sqlmaps.select ("rc_project").get ("name")
	q = sqlmaps.select ("rc_file", "t1").join (subq, "t2", t1__name = F ("t2.name"))
	q.filter (id__gt = 100)
	q.get ("score", "t2.name")
	assert q.query == (
		"SELECT score, t2.name FROM rc_file AS t1\n"
		"INNER JOIN (SELECT name FROM rc_project) AS t2 ON t1.name = t2.name\n"
		"WHERE id > 100"
	)
	sql = sqlmaps.update ("rc_file", "t1")
	sql.from_ ("rc_record", "t2", t1__id = F ("t2.id"))
	sql.data (score = F ("t2.score"))
	sql.filter (id = 1)

	assert sql.query == (
		"UPDATE rc_file AS t1 SET score = t2.score\n"
		"FROM rc_record AS t2 ON t1.id = t2.id\n"
		"WHERE id = 1"
	)

	sql = sqlmaps.update ("rc_file", "t1")
	sql.from_ ("rc_record")
	sql.data (score = F ("t2.score"))
	sql.filter (id = 1)

	assert sql.query == (
		"UPDATE rc_file AS t1 SET score = t2.score\n"
		"FROM rc_record\n"
		"WHERE id = 1"
	)

	sql = sqlmaps.update ("rc_file", "t1")
	sql.from_ ("rc_record t2")
	sql.data (score = F ("t2.score"))
	sql.filter (id = 1)

	assert sql.query == (
		"UPDATE rc_file AS t1 SET score = t2.score\n"
		"FROM rc_record t2\n"
		"WHERE id = 1"
	)

	sql = sqlmaps.update ("rc_file", "t1")
	sql.from_ ("rc_record", "t2")
	sql.data (score = F ("t2.score"))
	sql.filter (id = 1)

	assert sql.query == (
		"UPDATE rc_file AS t1 SET score = t2.score\n"
		"FROM rc_record AS t2\n"
		"WHERE id = 1"
	)

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql.all ()
	assert sql.query == (
		"SELECT score, count(*) cnt FROM rc_file\n"
		"WHERE 1 = 1"
	)
	sql.exclude (user = "hansroh", in_date__gt = datetime.datetime (2018, 5, 1, 0))
	assert sql.query.find ("WHERE 1 = 1 AND NOT (") !=- 1
	assert sql.query.find ("' AND") != -1

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql.filter (user = "hansroh")
	sql2 = sql.branch (None)
	sql.exclude (in_date__gt = datetime.datetime (2018, 5, 1, 0))
	sql.exclude (id__between = (100, 200))
	assert sql.query == (
		"SELECT score, count(*) cnt FROM rc_file\n"
		"WHERE user = 'hansroh' AND NOT (in_date > TIMESTAMP '2018-05-01 00:00:00') AND NOT (id BETWEEN 100 AND 200)"
	)
	assert sql2.query == (
		"SELECT score, count(*) cnt FROM rc_file\n"
		"WHERE user = 'hansroh'"
	)

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql.filter ("user = 'hansroh'")
	assert sql2.query == (
		"SELECT score, count(*) cnt FROM rc_file\n"
		"WHERE user = 'hansroh'"
	)
	sql.exclude ("id = 1")
	assert sql.query == (
		"SELECT score, count(*) cnt FROM rc_file\n"
		"WHERE user = 'hansroh' AND NOT (id = 1)"
	)
	sql.tran ()
	assert sql.as_sql () == (
		"BEGIN TRANSACTION;\n"
		"SELECT score, count(*) cnt FROM rc_file\n"
		"WHERE user = 'hansroh' AND NOT (id = 1);\n"
		"COMMIT;"
	)

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql2 = sqlmaps.select ("rc_file2")
	sql2.get ("score", "count(*) cnt")
	sql.union (sql2)
	assert sql.as_sql () == (
		"(SELECT score, count(*) cnt FROM rc_file)\n"
		"UNION\n"
		"(SELECT score, count(*) cnt FROM rc_file2)"
	)

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql2 = sqlmaps.select ("rc_file2")
	sql2.get ("score", "count(*) cnt")
	sql.union_all (sql2)
	assert sql.as_sql () == (
		"(SELECT score, count(*) cnt FROM rc_file)\n"
		"UNION ALL\n"
		"(SELECT score, count(*) cnt FROM rc_file2)"
	)

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql2 = sqlmaps.select ("rc_file2")
	sql2.get ("score", "count(*) cnt")
	sql.intersect (sql2)
	assert sql.as_sql () == (
		"(SELECT score, count(*) cnt FROM rc_file)\n"
		"INTERSECT\n"
		"(SELECT score, count(*) cnt FROM rc_file2)"
	)

	sql = sqlmaps.select ("rc_file")
	sql.get ("score", "count(*) cnt")
	sql2 = sqlmaps.select ("rc_file2")
	sql2.get ("score", "count(*) cnt")
	sql.except_ (sql2)
	assert sql.as_sql () == (
		"(SELECT score, count(*) cnt FROM rc_file)\n"
		"EXCEPT\n"
		"(SELECT score, count(*) cnt FROM rc_file2)"
	)
