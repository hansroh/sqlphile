from sqlphile import Q, D, F, V, DB_PGSQL

def test_cte_init (sqlmaps):
    sql = sqlmaps.with_ ("temp", sqlmaps.select ("tbl1 a").filter (a__id = 4))
    sql.select ("temp")

    assert str (sql) == (
        "WITH temp AS (SELECT * FROM tbl1 a\n"
        "WHERE a.id = 4)\n"
        "SELECT * FROM temp"
    )