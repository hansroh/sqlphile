import pytest

def test_no_condition (sqlmaps):
    with pytest.raises (AssertionError):
        sql = (sqlmaps.update ("tbl1")
            .data (a=4).execute ())

    with pytest.raises (AssertionError):
        sql = (sqlmaps.delete ("tbl1").execute ())

    with pytest.raises (AttributeError):
        sql = (sqlmaps.delete ("tbl1").all ().execute ())

