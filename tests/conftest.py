import sys, pytest
sys.path.insert (0, "..")
import sqlphile

@pytest.fixture (scope = "module")
def sqlmap ():
	s = sqlphile.Template ()
	s._read_from_string ("""
<sqlmap version="0.9b1">
	
<sql name="test1">
	select * from {tbl} WHERE {this.filter}
	{this.group_by}
	{this.order_by}
	{this.returning}
</sql>	

<sql name="test2">
	select * from {tbl}
</sql>
	""")
	return s
	
@pytest.fixture (scope = "module")
def sqlmaps ():
	return sqlphile.Template (path = "sqlmaps")
