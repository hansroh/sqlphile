from sqlphile import const

def test_const (sqlmaps):
    sql = (
      sqlmaps.select ("temp").const ("a", 1).const ('b', 'c').filter (a = const ("a"))
    )
    assert str (sql) == (
        "WITH a AS (values (1)), b AS (values ('c'))\n"
        "SELECT * FROM temp\n"
        "WHERE a = (table a)"
    )