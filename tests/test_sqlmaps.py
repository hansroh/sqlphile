import datetime
from sqlphile.q import Q, V

def test_SQLMap (sqlmap):
	assert len (sqlmap._sqls) == 2
	assert sqlmap._version == "0.9b1"

	sql = sqlmap.test1
	sql = sql.filter (name = 'Hans').group_by ("name").order_by ("name").feed (tbl = 'rc_file').returning ("id")
	assert sql.query == (
		"select * from rc_file WHERE name = 'Hans'\n"
		"GROUP BY name\n"
		"ORDER BY name\n"
		"RETURNING id"
	)

	sql = sqlmap.test1 (tbl = 'rc_file')
	sql = sql.filter (Q (id = 6), Q (email = None), name = 'Hans').group_by ("name").order_by ("name").returning ("id")
	assert sql.query == (
		"select * from rc_file WHERE id = 6 AND name = 'Hans'\n"
		"GROUP BY name\n"
		"ORDER BY name\n"
		"RETURNING id"
	)
