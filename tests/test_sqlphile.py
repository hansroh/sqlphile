import datetime
from sqlphile.q import Q, V

def test_SQLPhile (sqlmaps):
	sql = sqlmaps.test.test1.filter (name = 'Hans').group_by ("name").order_by ("name").feed (tbl = 'rc_file')
	assert sql.query == (
		"select * from rc_file WHERE name = 'Hans'\n"
		"GROUP BY name\n"
		"ORDER BY name"
	)

	sql = sqlmaps.test1.filter (name = 'Hans').group_by ("name").order_by ("name").feed (tbl = 'rc_file')
	assert sql.query == (
		"select * from rc_file WHERE name = 'Hans'\n"
		"GROUP BY name\n"
		"ORDER BY name"
	)

	sql = sqlmaps.template ("SELECT * FROM rc_file WHERE {a} AND {b} AND {c}")
	sql.feed (a = Q (a__neq = 1), b = Q (b__in = (1,2,3,4,5)), c = Q (c = None))
	assert sql.query == (
		"SELECT * FROM rc_file WHERE a <> 1 AND b IN (1,2,3,4,5) AND 1 = 1"
	)

	sql = sqlmaps.template ("SELECT * FROM rc_file WHERE a = {a_} AND b IN {b_}")
	sql.feed (a_ = V (__eq = 1), b_ = V (__in = (1,2,3,4,5)))
	assert sql.query == (
		"SELECT * FROM rc_file WHERE a = 1 AND b IN (1,2,3,4,5)"
	)

	sql = sqlmaps.template ("SELECT * FROM rc_file WHERE {this.filter}")
	sql.filter (a = 1, b__in = (1,2,3,4,5))
	assert sql.query in (
		"SELECT * FROM rc_file WHERE a = 1 AND b IN (1,2,3,4,5)",
		"SELECT * FROM rc_file WHERE b IN (1,2,3,4,5) AND a = 1"
	)
