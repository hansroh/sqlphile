from sqlphile import Q, D, F, V
import pytest

def test_join_update (sqlmaps):
    sql = (sqlmaps.update ("b")
                .from_ ('tbl2', 'b')
                .join ('tbl3', 'c', 'a.id = b.id')
                .set (b__k = F ('c.k')))

    assert str (sql) == (
        "UPDATE b SET b.k = c.k\n"
        "FROM tbl2 AS b\n"
        "INNER JOIN tbl3 AS c ON a.id = b.id"
    )

    sql.filter (c__y = 1)
    assert str (sql) == (
        "UPDATE b SET b.k = c.k\n"
        "FROM tbl2 AS b\n"
        "INNER JOIN tbl3 AS c ON a.id = b.id\n"
        "WHERE c.y = 1"
    )

    sql = (sqlmaps.update ('tbl1', "a")
                .from_ ('tbl2', 'b', 'a.id = b.id')
                .join ('tbl3', 'c', 'a.id = c.id')
                .set (b__k = F ('c.k')))

    assert str (sql) == (
        "UPDATE tbl1 AS a SET b.k = c.k\n"
        "FROM tbl2 AS b ON a.id = b.id\n"
        "INNER JOIN tbl3 AS c ON a.id = c.id"
    )