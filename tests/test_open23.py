from test_helper import create
from sqlphile.db3 import open2, open3
import sqlite3
import pytest
import gc

def test_open2 ():
    create ()
    conn = sqlite3.connect ("sqlite3.db3")
    with open2 (conn) as db:
        q = db.select ("test").get ("id, name")
        q.execute ()
        assert len (db.fetchall ()) == 2

        q = db.select ("test").get ("id, name").execute ()
        d = db.fetchall (True)[1]
        print (d)
        assert d.id == 2
        assert d ["id"] == 2
        assert d.name == "Jane Doe"

    with pytest.raises (sqlite3.ProgrammingError):
        q = db.select ("test").get ("id, name").execute ()

def test_open2_2 ():
    create ()
    conn = sqlite3.connect ("sqlite3.db3")
    with open2 (conn, auto_closing = False) as db:
        q = db.select ("test").get ("id, name")
        q.execute ()
        assert len (db.fetchall ()) == 2

        q = db.select ("test").get ("id, name").execute ()
        d = db.fetchall (True)[1]
        print (d)
        assert d.id == 2
        assert d ["id"] == 2
        assert d.name == "Jane Doe"

    q = db.select ("test").get ("id, name").execute ()
    db.close ()

def test_open3 ():
    create ()
    conn = sqlite3.connect ("sqlite3.db3")
    with open3 (conn) as db:
        q1 = db.select ("test").get ("id, name").execute ()
        q2 = db.select ("test").get ("id, name").execute ()

        assert len (q1.fetch ()) == 2
        d = q2.fetch ()[1]

        assert d.id == 2
        assert d ["id"] == 2
        assert d.name == "Jane Doe"

        with pytest.raises (AttributeError):
            db.commit ()


def test_open3_2 ():
    create ()
    conn = sqlite3.connect ("sqlite3.db3")
    with open3 (conn) as db:
        q = db.select ("test").get ("id, name").execute ()
        assert q.fetchall ()
        assert q.cursor is None

        q = db.select ("test").get ("id, name").execute ()
        assert q.fetchall ()
        assert q.cursor is None

        q = db.select ("test").get ("id, name").execute ()
        assert q.fetchone ()
        assert q.cursor is not None

        while q.fetchone ():
            pass
        assert q.cursor is None
        assert q.conn is None

    conn = sqlite3.connect ("sqlite3.db3")
    with open3 (conn) as db:
        q = db.delete ("test").filter (id = 100).execute ()
        q.commit ()
        assert q.cursor is None

        q = db.select ("test").get ("id, name").filter (id = 100).execute ()
        assert not q.fetch ()
        assert q.cursor is None

        q = db.insert ("test").set (id = 100, name = 'Hans Roh').execute ()
        q.rollback()
        assert q.cursor is None

        q = db.select ("test").get ("id, name").filter (id = 100).execute ()
        assert not q.fetch ()
        assert q.cursor is None

        q = db.insert ("test").set (id = 100, name = 'Hans Roh').execute ()
        q.commit ()
        assert q.cursor is None

        q = db.select ("test").get ("id, name").filter (id = 100).execute ()
        assert q.fetch ()
        assert q.cursor is None

        q = db.select ("test").get ("id, name").execute ()

    assert q.fetchn (1)
    while q.fetchn (1):
        pass
    assert q.cursor is None
    assert q.conn is None
