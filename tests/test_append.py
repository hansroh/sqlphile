from sqlphile import Q, D, F, V

def test_append (sqlmaps):
    sql = sqlmaps.select ("temp").filter (id = 1)
    sql.append (sqlmaps.insert ("temp").set (id = 2))
    assert str (sql) == (
        "SELECT * FROM temp\n"
        "WHERE id = 1;\n"
        "INSERT INTO temp (id) VALUES (2)"
    )


