import pytest
from sqlphile.model import AbstractModel
from sqlphile import SQLPhile

class BaseModel (AbstractModel):
    @classmethod
    def db (self, *args, **kargs):
        from sqlphile import SQLPhile
        return SQLPhile ()


class Model0 (BaseModel):
    @classmethod
    def get_table_name (cls):
        return "tbl"


class Model (BaseModel):
    @classmethod
    def get_table_name (cls):
        return "tbl"

    @classmethod
    def validate (cls, data, create = False):
        return create and {"b": 1} or {"a": 1}

    @classmethod
    def get_pk (cls):
        return 'id'


class Model2 (Model):
    @classmethod
    def get_table_name (cls):
        return "tbl2"

    @classmethod
    def get_fks (cls):
        return {'fk': ('fk_id', Model)}


class Model3 (Model2):
    @classmethod
    def get_table_name (cls):
        return "tbl3"

    @classmethod
    def get_fks (cls):
        return {'fk': ('fk_id', Model2)}

    @classmethod
    def get_columns (cls):
        return ['id', 'name']


class Model4 (BaseModel):
    @classmethod
    def get_table_name (cls):
        return "tbl4"

    @classmethod
    def get_fks (cls):
        return {'fk': ('fk_id', Model)}


class Model5 (Model4):
    @classmethod
    def get_table_name (cls):
        return "tbl5"

    @classmethod
    def validate (cls, data, create = False):
        return create and {"b": 1} or {"a": 1}


def test_tabled_column (sqlmaps):
    return
    q = sqlmaps.sql ()
    q.select (Model3, "a").get ("a.id, fk, replace (a.name, 'name', ' ') name, id||name name2")
    assert str (q) == (
        "SELECT a.id, fk_id, replace (a.name, 'name', ' ') name, id||name name2 FROM tbl3 AS a"
    )

def test_model (sqlmaps):
    return
    q = sqlmaps.sql ()
    q.insert (Model0).set (c = 1)
    assert str (q) == (
        "INSERT INTO tbl (c) VALUES (1)"
    )
    with pytest.raises (ValueError):
        q.set (d = 1)

    q = sqlmaps.sql ()
    q.select (Model, "a")
    assert str (q) == (
        "SELECT * FROM tbl AS a"
    )

    q = sqlmaps.sql ()
    q.insert (Model).set (c = 1)
    assert str (q) == (
        "INSERT INTO tbl (b) VALUES (1)"
    )
    with pytest.raises (ValueError):
        q.set (d = 1)

    q = sqlmaps.sql ()
    q.update (Model).set (c = 1)
    assert str (q) == (
        "UPDATE tbl SET a = 1"
    )
    q.set (d = 1)
    assert str (q) == (
        "UPDATE tbl SET a = 1"
    )

def test_join_related (sqlmaps):
    return
    q = sqlmaps.sql ()
    q.select (Model2, 'a').join_related ('fk', 'b').filter (fk = 4, b__lu = True)
    assert str (q) == (
        "SELECT * FROM tbl2 AS a\n"
        "INNER JOIN tbl AS b ON a.fk_id = b.id\n"
        "WHERE fk_id = 4 AND b.lu = true"
    )

    q = sqlmaps.sql ()
    q.select (Model2).full_join_related ('fk', 'b').filter (fk = 4, b__lu = True)
    assert str (q) == (
        "SELECT * FROM tbl2\n"
        "FULL OUTER JOIN tbl AS b ON tbl2.fk_id = b.id\n"
        "WHERE fk_id = 4 AND b.lu = true"
    )


def test_update_join_related (sqlmaps):
    return
    q = sqlmaps.sql ()
    (q.update (Model4).from_related ('fk', 'x')
            .set (this__name = 'Hans', fk = 4, x__lu = True)
            .filter (fk = 4, x__lu = True, x__lu__gt = 5))

    assert str (q) == (
        "UPDATE tbl4 SET tbl4.name = 'Hans', fk_id = 4, x.lu = true\n"
        "FROM tbl AS x\n"
        "WHERE tbl4.fk_id = x.id AND fk_id = 4 AND x.lu = true AND x.lu > 5"
    )

    q = sqlmaps.sql ()
    (q.update (Model4).from_related ('fk', 'x')
            .set (this__name = 'Hans', fk = 4, x__lu = True)
            .filter (fk = 4, x__lu = True, x__lu__gt = 5))

    assert str (q) == (
        "UPDATE tbl4 SET tbl4.name = 'Hans', fk_id = 4, x.lu = true\n"
        "FROM tbl AS x\n"
        "WHERE tbl4.fk_id = x.id AND fk_id = 4 AND x.lu = true AND x.lu > 5"
    )


def test_join_related2 (sqlmaps):
    return
    q = sqlmaps.sql ()
    (q.select (Model3)
        .join_related ('fk', 'b')
        .join_related ('fk__fk', 'c')
        .get ("this__id, id AS k, fk, b.id, b.id as x, indexof (c.name, 'x') as y")
        .filter (fk = 4, b__lu = True, c__lu = False)
        .filter (b__data___age = 20)
        .order_by ('this__id, -b.id', '+c.name')
        .group_by ('this__id, b.id', 'c.name'))

    assert str (q) == (
        "SELECT tbl3.id, id AS k, fk_id, b.id, b.id as x, indexof (c.name, 'x') as y FROM tbl3\n"
        "INNER JOIN tbl2 AS b ON tbl3.fk_id = b.id\n"
        "INNER JOIN tbl AS c ON b.fk_id = c.id\n"
        "WHERE fk_id = 4 AND b.lu = true AND c.lu = false AND CAST (b.data ->> 'age' AS int) = 20\n"
        "GROUP BY tbl3.id, b.id, c.name\n"
        "ORDER BY tbl3.id, b.id DESC, c.name"
    )

    q = sqlmaps.sql ()
    (q.select (Model3, 'k')
        .join_related ('fk', 'a')
        .join_related ('fk__fk', 'b')
        .get ("k.id, id AS k, fk, a.id, a.id as x, indexof (a.name, 'x') as y")
        .filter (fk = 4, a__lu = True, b__lu = False)
        .filter (a__data___age = 20)
        .order_by ('k.id, -a.id', '+a.name')
        .group_by ('k.id, a.id', 'a.name'))

    assert str (q) == (
        "SELECT k.id, id AS k, fk_id, a.id, a.id as x, indexof (a.name, 'x') as y FROM tbl3 AS k\n"
        "INNER JOIN tbl2 AS a ON k.fk_id = a.id\n"
        "INNER JOIN tbl AS b ON a.fk_id = b.id\n"
        "WHERE fk_id = 4 AND a.lu = true AND b.lu = false AND CAST (a.data ->> 'age' AS int) = 20\n"
        "GROUP BY k.id, a.id, a.name\n"
        "ORDER BY k.id, a.id DESC, a.name"
    )


def test_model_methods (sqlmaps):
    return
    q = Model5.get ()
    assert str (q) == (
        "SELECT * FROM tbl5"
    )

    q = Model5.get ().partial ('id, name')
    assert str (q) == (
        "SELECT id, name FROM tbl5"
    )

    q = Model5.get ().partial ('id, name').filter (id__gt = 100)
    assert str (q) == (
        "SELECT id, name FROM tbl5\n"
        "WHERE id > 100"
    )

    q = Model5.add (dict (c = 1))
    assert str (q) == (
        "INSERT INTO tbl5 (b) VALUES (1)"
    )

    q = Model5.set (dict (c = 1))
    assert str (q) == (
        "UPDATE tbl5 SET a = 1"
    )
    q = Model5.set (dict (c = 1)).filter (id__gt = 100)
    assert str (q) == (
        "UPDATE tbl5 SET a = 1\n"
        "WHERE id > 100"
    )

    q = Model5.remove ().filter (k = 1)
    assert str (q) == (
        "DELETE FROM tbl5\n"
        "WHERE k = 1"
    )

    q = Model5.remove (k = 1)
    assert str (q) == (
        "DELETE FROM tbl5\n"
        "WHERE k = 1"
    )
