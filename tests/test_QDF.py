from sqlphile import Q, D, F, V
import datetime
from sqlphile import DB_PGSQL, DB_SQLITE3
import pytest

def test_Q ():
	assert str (Q ()) == ""
	assert str (Q (id__all = True)) == "1 = 1"
	assert str (Q (__all = True)) == "1 = 1"
	assert str (Q (name = "Hans Roh")) == "name = 'Hans Roh'"
	assert str (Q (name__startswith = "Hans Roh")) == "name LIKE 'Hans Roh%'"
	assert str (Q (name__istartswith = "Hans Roh")) == "name ILIKE 'Hans Roh%'"
	assert str (Q (name__iendswith = "Hans Roh")) == "name ILIKE '%Hans Roh'"
	assert str (Q (name__startswith = "Hans%20Roh")) == "name LIKE 'Hans\\%20Roh%'"
	assert str (Q (name__nstartswith = "Hans%20Roh")) == "name NOT LIKE 'Hans\\%20Roh%'"
	assert str (Q (name__neq = "Hans")) == "name <> 'Hans'"
	assert str (Q (name__isnull = True)) == "name IS NULL"
	assert str (Q (name__isnull = False)) == "name IS NOT NULL"
	assert str (Q (name__exact = F ("a.name"))) == "name = a.name"
	assert str (Q (name__iexact = F ("a.name"))) == "name ILIKE a.name"
	assert str (Q (name__endswith = F ("a.name"))) == "name LIKE '%' || a.name"
	assert str (Q (name__iendswith = F ("a.name"))) == "name ILIKE '%' || a.name"
	assert str (~Q (name__exact = "Hans")) == "NOT (name = 'Hans')"
	assert str (~Q (name__exact = "Hans")) == "NOT (name = 'Hans')"
	assert str (Q (name__regex = "^Hans")) == "name ~ '^Hans'"
	assert str (Q (name__iregex = "^Hans")) == "name ~* '^Hans'"

	assert str (Q (date__year = 2023)) == "DATE_APRT('YEAR', date) = 2023"
	assert str (Q (date__day_of_year = 100)) == "DATE_APRT('DOY', date) = 100"
	assert str (Q (date__iso_week_day = 6)) == "DATE_APRT('ISODOW', date) = 6"
	assert str (Q (date__week_day = 6)) == "DATE_APRT('DOW', date) = 6"
	assert str (Q (date__quarter = 0)) == "DATE_APRT('QUARTER', date) = 0"
	assert str (Q (date__date = datetime.datetime (2030, 1, 30))) == "TO_CAHR(date, 'yyyy-mm-dd') = '2030-01-30'"
	assert str (Q (date__time = datetime.time (20, 1, 30))) == "TO_CAHR(date, 'hh24-mi-ss') = '20:01:30'"

	assert str (~Q (name__exact = "Hans", id=6)) in (
		"NOT ((id = 6 AND name = 'Hans'))",
		"NOT ((name = 'Hans' AND id = 6))"
	)
	assert str (Q (name = None)) == ""
	assert str (Q (a = 1) | ~Q (b = 1)) == "(a = 1 OR NOT (b = 1))"
	assert str (Q (a = 1) | ~Q (b = 1) | Q (c = None)) == "(a = 1 OR NOT (b = 1))"
	assert str (Q (a = 1) | Q (b = 1) | Q (c = 1)) == "((a = 1 OR b = 1) OR c = 1)"
	assert str (Q (a = 1) & (Q (b = 1) | Q (c = 1))) == "(a = 1 AND (b = 1 OR c = 1))"
	assert str (Q (a = 1, b = 1)) in  ("(a = 1 AND b = 1)", "(b = 1 AND a = 1)")
	assert str (Q (a__in = F ("'a'"))) == "a IN ('a')"
	assert str ((Q (a = 1) & Q (b = 1)) | Q (c = 1)) in ("((a = 1 AND b = 1) OR c = 1)", "((b = 1 AND a = 1) OR c = 1)")
	assert str (Q (str (Q (name = "Hans Roh"))) | Q (id = 1)) == "(name = 'Hans Roh' OR id = 1)"
	assert str (Q (tbl__name__startswith = "Hans%20Roh")) == "tbl.name LIKE 'Hans\\%20Roh%'"
	assert str (Q (tbl__data___name = "Hans Roh")) == "tbl.data ->> 'name' = 'Hans Roh'"
	assert str (Q (tbl__data___age = 20)) == "CAST (tbl.data ->> 'age' AS int) = 20"
	assert str (Q (tbl__data___person___age = 20)) == "CAST (tbl.data -> 'person' ->> 'age' AS int) = 20"
	assert str (Q (tbl__data___persons__3 = 20)) == "CAST (tbl.data #>> '{persons, 3}' AS int) = 20"
	assert str (Q (tbl__data___persons__3___age = 20)) == "CAST (tbl.data #> '{persons, 3}' ->> 'age' AS int) = 20"
	assert str (Q (tbl__data___persons__3___age__isnull = True)) == "CAST (tbl.data #> '{persons, 3}' ->> 'age' AS boolean) IS NULL"
	assert str (Q (tbl__data___persons__3___age__1__isnull = True)) == "CAST (tbl.data #> '{persons, 3}' #>> '{age, 1}' AS boolean) IS NULL"
	assert str (Q (tbl__data___persons__haskey = 'one')) == "tbl.data ->> 'persons' ? 'one'"
	assert str (Q (data__haskey = 'one')) == "data ? 'one'"
	with pytest.raises (AssertionError):
		assert str (Q (data__haskey = 1)) == "data ? 'one'"
	assert str (Q (tbl__data___persons__haskeyin = ('one', 'two'))) == "tbl.data ->> 'persons' ?| array ['one','two']"
	assert str (Q (data__haskeyin = ['one', 'two'])) == "data ?| array ['one','two']"

def test_V ():
	assert V (__eq = "Hans Roh").render () == "'Hans Roh'"
	assert V (datetime.date.today ()).render ().startswith ("TIMESTAMP '")
	assert V (datetime.date.today ()).render ("sqlite3").startswith ("'")
	assert str (V (__eq = "Hans Roh")) == "'Hans Roh'"
	assert str (V ("Hans Roh")) == "'Hans Roh'"
	assert str (V (1)) == "1"
	assert str (V (__contains = "Hans Roh")) == "'%Hans Roh%'"
	assert str (V (__in = [1,2,3])) == "(1,2,3)"
	assert str (V (__between = [1,2])) == "1 AND 2"
	assert str (V (__isnull = True)) == "NULL"
	assert str (V (None)) == "NULL"
	assert str (V ()) == "NULL"

def test_D (sqlmap):
	d = D (name = "Hans Roh")
	d.encode ("postgresql")
	assert d.columns == "name"
	assert d.values == "'Hans Roh'"
	assert d.pairs == "name = 'Hans Roh'"

	q = sql = sqlmap.test1
	q.data (additional = D (name = "Hans Roh"))
	assert q._data ["additional"].values == "'Hans Roh'"

	with pytest.raises (AssertionError):
		q.data (this = D (name = "Hans Roh"))

