from sqlphile.pg2 import Pool
from sqlphile import db3
import rs4
import pytest
import os

# db3 ----------------------------------------------
def query_db3 (p):
    with p.acquire () as db:
        cid = id (db.conn)
        db.execute ('SELECT 1')
        rows = db.fetch ()
        return cid

def test_pool_db3 ():
    p = db3.Pool (2, '.temp.db3')
    ids = set ()
    for _ in range (1000):
        ids.add (query_db3 (p))
    assert len (ids) == 1

def test_pool_db3_threaded ():
    p = db3.Pool (4, '.temp.db3')
    ids = set ()
    with rs4.tpool (4) as tp:
        fs = [tp.submit (query_db3, p) for _ in range (1000)]
    for f in rs4.as_completed (fs):
        ids.add (f.result ())
    assert len (ids) <= 4

def test_pool_db3_poolerror ():
    p = db3.Pool (2, '.temp.db3')
    ids = set ()
    with rs4.tpool (4) as tp:
        fs = [tp.submit (query_db3, p) for _ in range (1000)]
    raised = False
    for f in rs4.as_completed (fs):
        if f.exception ():
            with pytest.raises (db3.PoolError):
                raised = True
                raise f.exception ()
    assert raised

IS_GITLAB = os.getenv ("CI_PROJECT_ID")
# pg2 ----------------------------------------------
def query (p):
    with p.acquire () as db:
        cid = id (db.conn)
        db.execute ('''SELECT * FROM foo where from_wallet_id=8 or detail = 'ReturnTx' order by created_at desc limit 10;''')
        rows = db.fetch ()
        assert 'tx_id' in rows [0]
        return cid

def test_pool ():
    if IS_GITLAB: return
    p = Pool (2, 'skitai', 'skitai', '12345678', 'postgres')
    ids = set ()
    for _ in range (1000):
        ids.add (query (p))
    assert len (ids) == 1

def test_pool_threaded ():
    if IS_GITLAB: return
    p = Pool (4, 'skitai', 'skitai', '12345678', 'postgres')
    ids = set ()
    with rs4.tpool (4) as tp:
        fs = [tp.submit (query, p) for _ in range (1000)]
    for f in rs4.as_completed (fs):
        ids.add (f.result ())
    assert len (ids) < 200
