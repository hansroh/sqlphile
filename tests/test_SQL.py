from sqlphile import Q, D, F, V, DB_PGSQL

def test_SQL1 (sqlmaps):
    cte = sqlmaps.sql ()
    cte.select ("tbl1 a").filter (a__id = 4)
    sql = (
        sqlmaps.select ("temp").with_ ("temp", cte)
    )
    assert str (sql) == (
        "WITH temp AS (SELECT * FROM tbl1 a\n"
        "WHERE a.id = 4)\n"
        "SELECT * FROM temp"

    )

def test_SQL2 (sqlmaps):
    q = sqlmaps.sql ()
    q.with_ ('a', sqlmaps.select ('tbl1'))
    q.select ("tbl1 a").filter (a__id = 4)
    assert str (q) == (
        "WITH a AS (SELECT * FROM tbl1)\n"
        "SELECT * FROM tbl1 a\n"
        "WHERE a.id = 4"
    )
