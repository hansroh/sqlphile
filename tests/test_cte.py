from sqlphile import Q, D, F, V, DB_PGSQL

def test_cte (sqlmaps):
    cte = (sqlmaps.select ("tbl1 a")
        .filter (a__id = 4))
    sql = (
        sqlmaps.select ("temp").with_ ("temp", cte)
    )
    assert str (sql) == (
        "WITH temp AS (SELECT * FROM tbl1 a\n"
        "WHERE a.id = 4)\n"
        "SELECT * FROM temp"

    )

def test_cte_with_D (sqlmaps):
    cte = D (a = 1, b = "c").encode (DB_PGSQL)
    sql = (
        sqlmaps.select ("temp").with_ ("temp", cte)
    )
    assert str (sql) == (
        "WITH temp (a, b) AS (values (1, 'c'))\n"
        "SELECT * FROM temp"

    )

    cte = D (a = 1, b = "c")
    sql = (
        sqlmaps.select ("tbl").with_ ("temp", cte).join ("temp", "true")
    )
    print (sql)
    assert str (sql) == (
        "WITH temp (a, b) AS (values (1, 'c'))\n"
        "SELECT * FROM tbl\n"
        "INNER JOIN temp ON true"
    )

def test_multiple_cte (sqlmaps):
    cte = (sqlmaps.select ("tbl1 a")
        .filter (a__id = 4))
    sql = (
        sqlmaps.select ("temp").with_ ("temp", cte).with_ ("temp2", cte)
    )
    assert str (sql) == (
        "WITH temp AS (SELECT * FROM tbl1 a\n"
        "WHERE a.id = 4), temp2 AS (SELECT * FROM tbl1 a\n"
        "WHERE a.id = 4)\n"
        "SELECT * FROM temp"

    )

def test_multiple_cte2 (sqlmaps):
    cte = (sqlmaps.insert ('users').data (uid = 'UNITTEST').returning ('idx, uid'))
    cte2 = (sqlmaps.select ('newuser').get ('idx, uid').into ('rewards', 'user_idx, uid'))
    sql = (sqlmaps.select ('newuser').get ('uid')
        .with_ ('newuser', cte)
        .with_ ('inserted', cte2))

    assert str (sql) == (
        "WITH newuser AS (INSERT INTO users (uid) VALUES ('UNITTEST')\n"
        "RETURNING idx, uid), inserted AS (INSERT INTO rewards (user_idx, uid)\n"
        "SELECT idx, uid FROM newuser)\n"
        "SELECT uid FROM newuser"
    )

