from sqlphile import Q, D, F, V
import datetime
from sqlphile import DB_PGSQL, DB_SQLITE3

def test_I ():    
    assert str (Q (id_ = None)) == ""
    assert str (Q (id_ = None, a="x")) == "(a = 'x')"
    assert str (Q (id_ = None) | Q (a="x")) == "a = 'x'"
    assert str (~Q (id_ = None) | Q (a="x")) == "a = 'x'"
    assert str (Q (id__contains = None) | Q (a="x")) == "a = 'x'"
    assert str (Q (a="x") | Q (id__contains = None)) == "a = 'x'"
    assert str (~Q (id__contains = None) | Q (a="x")) == "a = 'x'"
    assert str (Q (id__contains = None) & Q (a="x")) == "a = 'x'"
    
def test_I_sql (sqlmaps):
    sql = sqlmaps.select ("rc_file")
    sql.filter (email = None, _id = 1)
    assert str (sql).find ("email") == -1    

    sql = sqlmaps.select ("rc_file")
    sql.exclude (email = None, _id = 1)
    assert str (sql).find ("email") == -1    
