from sqlphile import Q

def test_void (sqlmaps):
    sql = sqlmaps.select ("rc_file")	
    sql.filter (email = None, _id = 1).exclude (a = 1).exclude (b = None)
    assert str (sql) == (
        "SELECT * FROM rc_file\n"
        "WHERE _id = 1 AND NOT (a = 1)"
    )

    sql = sqlmaps.select ("rc_file")	
    sql.filter (email = None, _id = 1).exclude (a = 1, b = None)
    assert str (sql) == (
        "SELECT * FROM rc_file\n"
        "WHERE _id = 1 AND NOT (a = 1)"
    )

    sql = sqlmaps.select ("rc_file")
    sql.filter (email = None, _id = 1).exclude (Q (a = 1) | Q (b = None))
    assert str (sql) == (
        "SELECT * FROM rc_file\n"
        "WHERE _id = 1 AND NOT (a = 1)"
    )

    sql = sqlmaps.select ("rc_file")
    sql.filter (~Q (a = 1), email = None, _id = 1)
    assert str (sql) == (
        "SELECT * FROM rc_file\n"
        "WHERE NOT (a = 1) AND _id = 1"
    )

    sql = sqlmaps.select ("rc_file")	
    sql.filter ("", None, email = None, _id = 1).exclude (a = 1, b = None)
    assert str (sql) == (
        "SELECT * FROM rc_file\n"
        "WHERE _id = 1 AND NOT (a = 1)"
    )


