import sqlphile as sp
import os

def create ():
    if os.path.isfile ("sqlite3.db3"):
        os.remove ("sqlite3.db3")
        
    with sp.sqlite3 ("sqlite3.db3") as db:
        db.execute ("create table test (id int, name text)")
        db.commit ()        
        db.insert ("test").data (id = 1, name = "Hans Roh").execute ()
        db.insert ("test").data (id = 2, name = "Jane Doe").execute ()
        db.commit ()
            
def test_helpr ():
    create ()
    with sp.sqlite3 ("sqlite3.db3") as db:
        q = db.select ("test").get ("id, name")
        q.execute ()        
        assert len (db.fetchall ()) == 2
        
        q = db.select ("test").get ("id, name").execute ()
        d = db.fetchall (True)[1]
        print (d)
        assert d.id == 2
        assert d ["id"] == 2
        assert d.name == "Jane Doe"
        
def test_sqlmap ():     
    create ()
    with sp.sqlite3 ("sqlite3.db3", "./sqlmaps") as db:
        q = db.test1.filter (name = 'Hans Roh').group_by ("name").order_by ("name").feed (tbl = 'test').returning ("id")
        assert str (q).startswith ("select * from test WHERE name = 'Hans Roh'")
        q.execute ()
        assert db.fetchall (True)[0]["name"] == "Hans Roh"
        
        q = db.test1.filter (name = 'Hans Roh').group_by ("name").order_by ("name").feed (tbl = 'test').returning ("id")
        q.execute ().fetchall (True)[0]["name"] == "Hans Roh"

def test_execute_and_fetch ():
    create ()
    with sp.sqlite3 ("sqlite3.db3") as db:
        r = db.select ("test").get ("id, name").execute ().fetchall ()
        assert len (r) == 2
        
        db.select ("test").get ("id, name").execute ()
        r = db.fetchall ()
        assert len (r) == 2
        
        db.execute ("select id, name from test")
        r = db.fetchall ()
        assert len (r) == 2
        
        r = db.execute ("select id, name from test").fetchall ()
        assert len (r) == 2

                










        