def test_operation (sqlmaps):
    sql = sqlmaps.insert ('test').set (box = [5,6,7,8], name = ['a', 'b', 'c'])
    assert str (sql) == "INSERT INTO test (box, name) VALUES ('{5,6,7,8}', '{a,b,c}')"

    sql = sqlmaps.update ('test').set (box = [5,6,7,8], name = ['a', 'b', 'c'])
    assert str (sql) == "UPDATE test SET box = '{5,6,7,8}', name = '{a,b,c}'"
