"""
Hans Roh 2015 -- http://osp.skitai.com
License: BSD
"""
import re
import sys
import os
import shutil, glob
import codecs
from warnings import warn
try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

with open('sqlphile/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*"(.*?)"',fd.read(), re.M).group(1)

if 'publish' in sys.argv:
	os.system ('{} setup.py bdist_wheel'.format (sys.executable))
	whl = glob.glob ('dist/sqlphile-{}-*.whl'.format (version))[0]
	os.system ('twine upload {}'.format (whl))
	sys.exit ()

classifiers = [
  'License :: OSI Approved :: MIT License',
  'Development Status :: 4 - Beta',
  'Topic :: Database',
	'Intended Audience :: Developers',
	'Programming Language :: Python',
	'Programming Language :: Python :: 3',
]

packages = [
	'sqlphile',
]

package_dir = {'sqlphile': 'sqlphile'}
package_data = {}

install_requires = [
	"rs4>=0.3"
]

if __name__ == "__main__":
	with codecs.open ('README.rst', 'r', encoding='utf-8') as f:
		long_description = f.read()

	setup(
		name='sqlphile',
		version=version,
		description='SQL Phile',
		long_description=long_description,
		url = 'https://gitlab.com/skitai/sqlphile',
		author='Hans Roh',
		author_email='hansroh@gmail.com',
		packages=packages,
		package_dir=package_dir,
		package_data = package_data,
		license='MIT',
		platforms = ["posix", "nt"],
		download_url = "https://pypi.python.org/pypi/sqlphile",
		install_requires = install_requires,
		classifiers=classifiers
	)
